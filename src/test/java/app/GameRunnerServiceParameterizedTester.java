package app;

import static org.junit.Assert.assertEquals;

import app.model.domain.Room;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class GameRunnerServiceParameterizedTester {

  private Room[] input;
  private boolean expectedResult;

  public GameRunnerServiceParameterizedTester(Room[] input, boolean expectedResult) {
    this.input = input;
    this.expectedResult = expectedResult;
  }

  @Parameters
  public static List<Object[]> isAnyUncheckedRoomsMethodData() {
    Object[][] rooms = new Object[3][2];
    for (int i = 0; i < rooms.length; i++) {
      rooms[i] = new Room[]{new Room(null), new Room(null)};
    }
    ((Room) rooms[0][0]).setCheckedByPlayer(true);
    ((Room) rooms[0][1]).setCheckedByPlayer(false);
    ((Room) rooms[1][0]).setCheckedByPlayer(true);
    ((Room) rooms[1][1]).setCheckedByPlayer(true);
    ((Room) rooms[2][0]).setCheckedByPlayer(false);
    ((Room) rooms[2][1]).setCheckedByPlayer(false);

    return Arrays.asList(new Object[][]{
        {rooms[0], true},
        {rooms[1], false},
        {rooms[2], true}
    });
  }

  @Test
  public void IsAnyUncheckedRoomsMethodTest() {
    assertEquals(expectedResult, GameRunnerService.isAnyUncheckedRooms(input));
  }

}
