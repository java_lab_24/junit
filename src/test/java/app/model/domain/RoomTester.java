package app.model.domain;

import static org.hamcrest.CoreMatchers.is;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RoomTester {

  private Player testPlayer;
  private Monster monster;
  private Artifact artifact;

  @Before
  public void setUp() {
    testPlayer = new Player("test");
    monster = Monster.BLOODFEN_RAPTOR;
    artifact = Artifact.DAEDALUS;
  }

  @Test
  public void UpdatePlayerInTheRoomWithMonster() {
    Room room = new Room(monster);
    int playerPowerBefore = testPlayer.getPower();
    room.updatePlayerPower(testPlayer);
    int playerPowerAfter = playerPowerBefore - monster.getPower();
    Assert.assertThat(playerPowerAfter, is(testPlayer.getPower()));
  }

  @Test
  public void UpdatePlayerInTheRoomWithArtifact() {
    Room room = new Room(artifact);
    int playerPowerBefore = testPlayer.getPower();
    room.updatePlayerPower(testPlayer);
    int playerPowerAfter = playerPowerBefore + artifact.getPower();
    Assert.assertThat(playerPowerAfter, is(testPlayer.getPower()));
  }
}
