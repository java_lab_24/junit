package app;

import app.model.domain.Artifact;
import app.model.domain.Monster;
import app.model.domain.Player;
import app.model.domain.Room;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class GameRunnerServiceTester {

  Player player;
  Room room;
  Artifact testArtifact;
  Monster testMonster;

  @Before
  public void setUp() {
    player = new Player("test");
    testArtifact = Artifact.DAEDALUS;
    testMonster = Monster.BLOODFEN_RAPTOR;
  }

  @Test
  public void EnterToTheRoomWithMonsterTest() throws CloneNotSupportedException {
    room = new Room(testMonster);
    Player playerBefore = player.clone();
    Room roomBefore = room.clone();
    GameRunnerService.enterToTheRoom(player, room);
    Assert.assertFalse(playerBefore.equals(player));
    Assert.assertFalse(roomBefore.equals(room));
  }

  @Test
  public void EnterToTheRoomWithArtifactTest() throws CloneNotSupportedException {
    room = new Room(testArtifact);
    Player playerBefore = player.clone();
    Room roomBefore = room.clone();
    GameRunnerService.enterToTheRoom(player, room);
    Assert.assertFalse(playerBefore.equals(player));
    Assert.assertFalse(roomBefore.equals(room));
  }

  @Test(expected = NullPointerException.class)
  public void EnterToTheRoomNullPointerTest() {
    room = new Room(null);
    GameRunnerService.enterToTheRoom(player, room);
  }

  @Test
  public void GetSuccessTrackWithHoleMonstersRoomsTest() {
    Room[] rooms = new Room[10];
    for (int i = 0; i < rooms.length; i++) {
      rooms[i] = new Room(testMonster);
    }
    Assert.assertNull(GameRunnerService.getSuccessTrack(rooms));
  }

  @Test
  public void GetSuccessTrackWithHoleArtifactsRoomsTest() {
    Room[] rooms = new Room[10];
    for (int i = 0; i < rooms.length; i++) {
      rooms[i] = new Room(testArtifact);
    }
    Assert.assertNotNull(GameRunnerService.getSuccessTrack(rooms));
  }
}
