package app;

import app.model.domain.Artifact;
import app.model.domain.Monster;
import app.model.domain.Player;
import app.model.domain.Room;

import java.util.Scanner;

public class GameRunner {

    private static Player player;
    private static Room[] rooms = new Room[10];
    private static Scanner scn = new Scanner(System.in);
    private static int playerMenuInput;

    static {
        System.out.print("Welcome to Adventure Game\n" +
                "Please enter your name: ");
        String name = scn.nextLine();
        player = new Player(name);
        System.out.print("\nHappy to see you there " + name + ", lets rock!");
    }

    public static void main(String[] args) {
        while (playerMenuInput != -1) {
            MenuTextPrinter.printMainMenu();
            playerMenuInput = scn.nextInt();
            switch (playerMenuInput) {
                case 1:
                    player.setPower(25);
                    rooms = GameRunnerService.generateRooms();
                    MenuTextPrinter.printStartGame();
                    while (GameRunnerService.isAnyUncheckedRooms(rooms)) {
                        System.out.println("\n\n****************************************************************");
                        MenuTextPrinter.printRooms(rooms);
                        MenuTextPrinter.printPlayerInfo(player);
                        System.out.print("\nChoose the door: ");
                        playerMenuInput = scn.nextInt();
                        if (playerMenuInput == 11231) {
                            int[] successTrack = GameRunnerService.getSuccessTrack(rooms);
                            if (successTrack == null) {
                                System.out.println("\nUnfortunately in this game no success track");
                            } else {
                                MenuTextPrinter.printSuccessTrack(successTrack);
                            }
                        } else {
                            GameRunnerService.enterToTheRoom(player, rooms[playerMenuInput - 1]);
                            MenuTextPrinter.printRoomInfo(rooms[playerMenuInput - 1]);
                        }
                        if (player.getPower() < 0) {
                            System.out.println("\nYour power less then zero. You have lost!");
                            return;
                        }
                        System.out.println("****************************************************************");
                    }
                    System.out.println("\nYou entered to all rooms and steel alive");
                    System.out.print("\n!---You are the winner---!");
                    break;
                case 2:
                    break;
                case 0:
                    playerMenuInput = -1;
                    break;
            }
        }
    }

    private static class MenuTextPrinter {

        public static void printMainMenu() {
            System.out.print("\n\n1. Start new game\n" +
                    "2. About game\n" +
                    "0. Exit\n\n" +
                    "Your choose: ");
        }

        public static void printStartGame() {
            System.out.print("\n'You are the traveller\n" +
                    "and suddenly you realize that you are " +
                    "\nin an absolutely unfamiliar old castle.'" +
                    "\n\n'Since you are interested in medieval\n" +
                    "architecture nothing can prevent you from " +
                    "\nentering to this historical building and\n" +
                    "explore it.'" +
                    "\n\n'After long exploration you stacked\n" +
                    "with an unusual room which has 10 closed doors" +
                    "\n\n'So here where real adventure starts because\n" +
                    "behind of these doors monster or useful artifact " +
                    "\nwaiting for you...'");
        }

        public static void printRooms(Room[] rooms) {
            System.out.println("Here are doors:\n");
            for (int i = 0, doorIndex = 1; i < rooms.length; i++, doorIndex++) {
                if (!rooms[i].isCheckedByPlayer()) {
                    System.out.print("|" + doorIndex + "|  ");
                }
            }
            System.out.println();
        }

        public static void printPlayerInfo(Player player) {
            System.out.println("\nPlayer information");
            System.out.println("============================");
            System.out.println("player name: " + player.getName() +
                    "\nplayer power: " + player.getPower());
            System.out.println("============================");
        }

        public static void printRoomInfo(Room room) {
            if (room.getRoomObject() instanceof Artifact) {
                System.out.print("\nYou are super lucky because you find an" +
                        " artifact there!\n\n");
                System.out.println("------------------------------");
                System.out.println("Name: " + ((((Artifact) room.getRoomObject()).getName())));
                System.out.println("------------------------------");
                System.out.println("Description: " + ((((Artifact) room.getRoomObject()).getDescription())));
                System.out.println("------------------------------");
                System.out.println("Power: " + ((((Artifact) room.getRoomObject()).getPower())));
                System.out.println("------------------------------");
                System.out.println("Rarity: " + ((((Artifact) room.getRoomObject()).getRarity())));
                System.out.println("------------------------------");
            } else {
                System.out.print("\nBe careful it is a monster in the room!\n\n");
                System.out.println("------------------------------");
                System.out.println("Name: " + ((((Monster) room.getRoomObject()).getName())));
                System.out.println("------------------------------");
                System.out.println("Description: " + ((((Monster) room.getRoomObject()).getDescription())));
                System.out.println("------------------------------");
                System.out.println("Power: " + ((((Monster) room.getRoomObject()).getPower())));
                System.out.println("------------------------------");
            }
        }

        public static void printSuccessTrack(int[] successTrack) {
            System.out.println("\nSuccess track:");
            for (int i = 0; i < successTrack.length; i++) {
                if (successTrack[i] != 0) {
                    System.out.print("|" + successTrack[i] + "|  ");
                }
            }
            System.out.println("\nIf you follow this order you will winn");
        }
    }
}
