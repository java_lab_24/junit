package app.model.domain;

public class Player implements Cloneable {
    private String name;
    private int power;

    public Player(String name) {
        this.name = name;
        power = 25;
    }

    public String getName() {
        return name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Player
        && this.name.equals(((Player) obj).name)
        && this.power == ((Player) obj).power;
    }

    @Override
    public Player clone() throws CloneNotSupportedException {
        return (Player) super.clone();
    }
}
