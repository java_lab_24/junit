package app.model.domain;

public enum Monster {
    MAGMA_RAGER("Magma Rager", "Magma Ragers are height level" +
            "elite mobs found in Ulduar.", 10),
    BLOODFEN_RAPTOR("Bloodfen Raptor", "Raptors are one of the most " +
            "iconic beasts of the universe and can be found both in Azeroth " +
            "and Outland." +
            "Dustwallow Marsh.", 20),
    MURLOCK_RIDER("Marlock Rider", "The murloc is a bipedal, amphibious" +
            " humanoid race residing along coastlines, lakeshores, and riverbeds. Murlocs" +
            " possess bulbous bodies, large mouths lined with rows of sharp fangs, and " +
            "slime-coated skin.", 30);

    private String name;
    private String description;
    private int power;

    Monster(String name, String description, int power) {
        this.name = name;
        this.description = description;
        this.power = power;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getPower() {
        return power;
    }
}
