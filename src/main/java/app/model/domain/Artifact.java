package app.model.domain;

public enum Artifact {
    POWER_BOTTLE("Power Bottle", "Consumes a charge " +
            "to add 15 points of power", 15, Rarity.USUAL),
    OGRE_AXE("Ogre Axe", "This is an powerful axe " +
            "of the elder ogre monsters", 20, Rarity.USUAL),
    JAVALIN("Javalin", "A rather typical spear that " +
            "can sometimes pierce through an enemy's armor when used " +
            "to attack.", 30, Rarity.RARE),
    OCTARIN_CORE("Octarin Core", "25% of spell damage " +
            "dealt to enemy heroes is returned to the caster as health, " +
            "regardless of spell damage type.", 50, Rarity.SUPER_RARE),
    DAEDALUS("Daedalus", "Super rare item which makes a hero" +
            "almost unlimited power", 70, Rarity.SUPER_RARE);

    private String name;
    private String description;
    private int power;
    private Rarity rarity;

    Artifact(String name, String description, int power, Rarity rarity) {
        this.name = name;
        this.description = description;
        this.power = power;
        this.rarity = rarity;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getPower() {
        return power;
    }

    public Rarity getRarity() {
        return rarity;
    }

    enum Rarity {USUAL, RARE, SUPER_RARE}
}
