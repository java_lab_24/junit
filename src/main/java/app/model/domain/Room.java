package app.model.domain;

public class Room implements Cloneable {

    private Object roomObject;
    private boolean checkedByPlayer = false;

    public Room(Object roomObject) {
        this.roomObject = roomObject;
    }

    public boolean isCheckedByPlayer() {
        return checkedByPlayer;
    }

    public Object getRoomObject() {
        return roomObject;
    }

    public void setCheckedByPlayer(boolean checkedByPlayer) {
        this.checkedByPlayer = checkedByPlayer;
    }

    public void updatePlayerPower(Player player) {
        if (roomObject instanceof Artifact) {
            player.setPower(player.getPower() + ((Artifact) roomObject).getPower());
        } else {
            player.setPower(player.getPower() - ((Monster) roomObject).getPower());
        }
    }

    @Override
    public Room clone() throws CloneNotSupportedException {
        return (Room) super.clone();
    }

  @Override
  public boolean equals(Object obj) {
    return obj instanceof Room
        && this.roomObject.equals(((Room) obj).roomObject)
        && this.checkedByPlayer == ((Room) obj).checkedByPlayer;
  }
}
