package app;

import app.model.domain.Artifact;
import app.model.domain.Monster;
import app.model.domain.Player;
import app.model.domain.Room;

import java.util.Random;


public class GameRunnerService {

    public static Room[] generateRooms() {
        Room[] rooms = new Room[10];
        Random random = new Random();
        for (int i = 0; i < rooms.length; i++) {
            if ((random.nextInt(1000) % 2) == 0) {
                Artifact[] artifacts = Artifact.values();
                rooms[i] = new Room(artifacts[random.nextInt(artifacts.length - 1)]);
            } else {
                Monster[] monsters = Monster.values();
                rooms[i] = new Room(monsters[random.nextInt(monsters.length - 1)]);
            }
        }
        return rooms;
    }

    public static boolean isAnyUncheckedRooms(Room[] rooms) {
        for (Room r : rooms) {
            if (!r.isCheckedByPlayer()) {
                return true;
            }
        }
        return false;
    }

    public static void enterToTheRoom(Player player, Room room) {
        room.updatePlayerPower(player);
        room.setCheckedByPlayer(true);
    }

    public static int[] getSuccessTrack(Room[] rooms) {
        int[] successTrack = new int[10];
        Room[] tmpRooms = cloneRooms(rooms);
        Player player = new Player("cheater");
        for (int i = 0; i < tmpRooms.length; i++) {
            for (int j = 0, k = 0; j < tmpRooms.length; j++) {
                if (!tmpRooms[j].isCheckedByPlayer()) {
                    if (enterToTheRoomWithoutChangingPower(player, tmpRooms[j])) {
                        enterToTheRoom(player, tmpRooms[j]);
                        successTrack[k] = j + 1;
                        k++;
                    }
                }
            }
        }
        return getRealLength(successTrack) == rooms.length ? successTrack : null;
    }

    private static int getRealLength(int[] array) {
        int realLength = 0;
        int i = 0;
        while ((array[i] > 0) && (i < array.length - 1)) {
            realLength++;
            i++;
        }
        return realLength + 1;
    }

    private static boolean enterToTheRoomWithoutChangingPower(Player player, Room room) {
        Player tmpPlayer = player;
        Room tmpRoom = room;
        tmpRoom.updatePlayerPower(tmpPlayer);
        return (player.getPower() > 0);
    }

    private static Room[] cloneRooms(Room[] rooms) {
        Room[] cloneOfRooms = new Room[rooms.length];
        for (int i = 0; i < rooms.length; i++) {
            try {
                cloneOfRooms[i] = rooms[i].clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
        return cloneOfRooms;
    }
}